# docker

CI image for running docker operations. Unlike docker.io/docker this isn't built on alpine but instead comes with the full GNU toolset at your disposal.

(We also sneak go into this image because it's used for running [complement](https://github.com/matrix-org/complement) tests on a docker image)
